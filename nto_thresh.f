      module nto_data
      implicit none
      integer                        :: nSym,nFrag,nStates,nOcc
      integer                        :: nras1,nras3
      integer                        :: iRC,istat,lwork,ldiag
      integer                        :: nnto      
      integer,allocatable            :: nStatesFrag(:)
      integer,allocatable            :: nBasFrag(:),nOrbFrag(:)
      integer,allocatable            :: nInactFrag(:),nActFrag(:)
      integer,allocatable            :: nDetFrag(:)      
      character(len=25)              :: gsfile,excfile,detfile
      character(len=25)              :: ntofile, ntodetfile, gsdetfile
      character(len=25)              :: runfile,oneintfile
      character(len=255),allocatable :: detocc(:)
      real(kind=8)                   :: thresh_CI,thresh_NTO
      real(kind=8),allocatable       :: gs(:,:),exc(:,:),occNu(:)
      real(kind=8),allocatable       :: hole(:,:),part(:,:)      
      real(kind=8),allocatable       :: tmatrix(:,:)
      real(kind=8),allocatable       :: detcoef(:)
      real(kind=8),allocatable       :: sAO(:,:),sHP(:,:)
      real(kind=8),allocatable       :: u(:,:),vt(:,:),work(:),sval(:)
      end module
      
      
      subroutine read_vecs(iFrag)      
      use nto_data
      implicit none
      integer                   :: iFrag
      integer                   :: i,j
      integer                   :: ndet_raw      
      real(kind=8),allocatable  :: detcoef_raw(:)
      character(len=6)          :: mark
      character(len=132)        :: line
      character(len = 1 )       :: orbLabel(nBasFrag(iFrag))            
      character(len=255),allocatable       :: detocc_raw(:)

      nOcc = 0
      gs = 0.0
      exc = 0.0
      occNu = 0.0
      mark  = '#INDEX'
      open(9,file=gsfile, status='old')      
 46   read(9,'(A132)') line      
      if (line(1:6).ne.mark) goto 46
      read(9,'(A132)') line
      orbLabel = ' '
      read(9,'(2x,10A)')(orbLabel(i),i=1,nBasFrag(iFrag))      
      do i = 1, nBasFrag(iFrag)
        if (orbLabel(i) .eq. 'f') nOcc = nOcc + 1 ! frozen in RASSCF
        if (orbLabel(i) .eq. 'i') then
          nOcc = nOcc + 1       ! inactive          
        endif        
      end do
      rewind(9)
      mark = '#ORB'
 47   read(9,'(A132)') line
      if (line(1:4).ne.mark) goto 47
      do i = 1, nBasFrag(iFrag)
        read(9,'(A132)') line
        read(9,'(5E22.14)') (gs(i,j),j=1,nBasFrag(iFrag))
      end do

      close(unit=9)                              

      open(10,file=excfile,status='old')            
            
      nOcc = 0
      nOrbFrag(iFrag) = 0
      nInactFrag(iFrag) = 0
      nActFrag(iFrag) = 0
      nRas1 = 0
      nRas3 = 0
      mark = '#INDEX'
 48   read(10,'(A132)') line
      if (line(1:6).ne.mark) goto 48
      read(10,'(A132)') line
      orbLabel = ' '
      read(10,'(2x,10A)')(orbLabel(i),i=1,nBasFrag(iFrag))      
      do i = 1, nBasFrag(iFrag)
        if (orbLabel(i) .eq. 'f') nOcc = nOcc + 1     ! frozen in RASSCF
        if (orbLabel(i) .eq. 'i') then
          nOcc = nOcc + 1       ! inactive
          nInactFrag(iFrag) = nInactFrag(iFrag) + 1
          nOrbFrag(iFrag) = nOrbFrag(iFrag) + 1
        endif
        if (orbLabel(i) .eq. '1') then
          nOcc = nOcc + 1       ! active (ras1)
          nActFrag(iFrag) = nActFrag(iFrag) + 1
          nOrbFrag(iFrag) = nOrbFrag(iFrag) + 1
          nRas1 = nRas1 + 1
        endif
        if (orbLabel(i) .eq. '2') then
          nOcc = nOcc + 1       ! active (ras2)
          nActFrag(iFrag) = nActFrag(iFrag) + 1
          nOrbFrag(iFrag) = nOrbFrag(iFrag) + 1
        endif
        if (orbLabel(i) .eq. '3') then
          nOcc = nOcc + 1       ! active (ras3)
          nActFrag(iFrag) = nActFrag(iFrag) + 1
          nOrbFrag(iFrag) = nOrbFrag(iFrag) + 1
          nRas3 = nRas3 +1
        endif
        if (orbLabel(i) .eq. 's') then
          nOrbFrag(iFrag) = nOrbFrag(iFrag) + 1
        endif
      end do
      rewind(10)
      mark = '#ORB'
 49   read(10,'(A132)') line
      if (line(1:4).ne.mark) goto 49
      do i = 1, nBasFrag(iFrag)
        read(10,'(A132)') line
        read(10,'(5E22.14)') (exc(i,j),j=1,nBasFrag(iFrag))
      end do
      rewind(10)
      mark = '#OCC'
 50   read(10,'(A132)') line
      if (line(1:4).ne.mark) goto 50
      read(10,*) line
      read(10,'(5e22.14)')(occNu(j),j=1,nBasFrag(iFrag))      
      close(unit=10)      
      open(11,file=detfile, status='old')
      ndet_raw = 0
      
      read(11,*)
      do
        read(11,*,iostat=istat)
        if(istat.ne.0)then
          rewind(11)
          exit
        else
          ndet_raw = ndet_raw + 1          
        endif
      enddo            
      allocate(detcoef_raw(ndet_raw) )      
      allocate(detocc_raw(ndet_raw) )
      read(11,*)
      do i = 1,ndet_raw
        read(11,*) detcoef_raw(i),detocc_raw(i)
      enddo      
      nDetFrag(iFrag) = 0
      do i=1,ndet_raw
        if(abs(detcoef_raw(i)).ge.thresh_CI)then
          nDetFrag(iFrag) = nDetFrag(iFrag) + 1          
        endif
      enddo
      allocate(detcoef(nDetFrag(iFrag)) )      
      allocate(detocc(nDetFrag(iFrag)) )
      j=0
      do i=1,ndet_raw
        if(abs(detcoef_raw(i)).ge.thresh_CI)then
          j = j+1
          detcoef(j)=detcoef_raw(i)
          detocc(j)=detocc_raw(i)
        endif
      enddo
      deallocate(detcoef_raw,detocc_raw)
      write(6,*)'number of CI reduced from',ndet_raw,'to',
     &     nDetFrag(iFrag),'for fragment',iFrag
      write(6,*)'*DETERMINANT LIST*'
      do i = 1,nDetFrag(iFrag)
        write(6,*) i,detcoef(i),trim(detocc(i))
      enddo
      close(unit=11)

      end subroutine read_vecs

      subroutine getNTO(iFrag,iState)
      use nto_data
      implicit none
      integer            :: iFrag,iState
      integer            :: i,j,k,l
      integer            :: m,n      
      integer            :: iOcc,iVir,spin,ndiff
      integer            :: detheader(8)
      real(kind=8) :: dum
      real(kind=8),allocatable :: aux(:,:)
      
      character(len=255) :: occStr
            
      allocate(tmatrix(nRas1,nRas3))
      allocate(hole(nRas1,nBasFrag(iFrag)),part(nRas3,nBasFrag(iFrag)) )
      tmatrix = 0.0
      write(6,'(A,I2,A,I5)')'N BAS FRAG',iFrag,':',nBasFrag(iFrag)
      write(6,'(A,I3)')'RAS 1 OCCUPIED ORBITALS:',nRas1
      write(6,'(A,I3)')'RAS 3 VIRTUAL ORBITALS:',nRas3
      
      occStr = detocc(1)
      spin=1
      do i=1,nActFrag(iFrag)
        if(occstr(i:i).eq.'a')spin=spin+1
        if(occstr(i:i).eq.'b')spin=spin-1
      enddo
      write(6,'(A,I1)')'STATE:',iState
      write(6,'(A,I1)')'SPIN:',spin
      do i=1,nDetFrag(iFrag)
        write(6,'(A,I5,A,I5)')'Determinant',i,'   of',nDetFrag(iFrag)
        iOcc=0
        iVir=0
        occStr = detocc(i)
        ndiff=0
        write(6,'(A)')'occstring',occStr(1:nActFrag(iFrag))        
        if(spin.eq.1)then
          do j=1,nRas1
            if(occStr(j:j).eq.'b')then
              iOcc=j            
              ndiff=ndiff+1
            endif
          enddo
          do j=nRas1+1,nRas1+nRas3
            if(occStr(j:j).eq.'a')then
              iVir=j            
              ndiff=ndiff+1
            endif
          enddo
        else if(spin.eq.3)then
          do j=1,nRas1
            if(occStr(j:j).eq.'a')then
              iOcc=j
              ndiff=ndiff+1
            endif
          enddo
          do j=nRas1+1,nRas1+nRas3
            if(occStr(j:j).eq.'a')then
              iVir=j            
              ndiff=ndiff+1
            endif
          enddo
          do j=1,nRas1+nRas3
            if(occStr(j:j).eq.'b')ndiff=ndiff+1            
          enddo
        else
          write(6,'(A)')'spin configuration cannot
     &         be described with single excitations'
          write(6,'(A,I1)')'wrong spin:',spin
        endif
        if(iOcc.ne.0.and.iVir.ne.0.and.ndiff.eq.2)then        
          write(6,'(A,I3,A,I3)')'Excitation',iOcc,'  to',iVir
          write(6,*)'with coefficient',detcoef(i)
          tmatrix(iOcc,iVir-nRas1)=
     &         tmatrix(iOcc,iVir-nRas1)+abs(detcoef(i))          
        else
          write(6,'(A)')'No single excitation'
        endif
        write(6,*)'----------------------------------------------'
      enddo            
      write(6,'(A,I2,1x,I2)')'1-TDM FOR FRAGMENT:',iFrag,iState
      do i=1,nRas1
        write(6,'(200F7.3)')(tmatrix(i,j),j=1,nRas3)        
      enddo            
      if(nRas3.gt.nRas1)then
        ldiag = nRas1
        m = nRas3
        n = nRas1
        allocate(aux(nRas3,nRas1))
        do i=1,nRas3
          do j=1,nRas1
            aux(i,j)=tmatrix(j,i)
          enddo
        enddo
      else        
        ldiag = nRas3
        m = nRas1
        n = nRas3
        allocate(aux(nRas1,nRas3))
        do i=1,nRas1
          do j=1,nRas3
            aux(i,j)=tmatrix(i,j)
          enddo
        enddo
      endif
            
      allocate(u(m,m),vt(n,n))
      allocate(work(5*max(m,n)),sval(ldiag))
      u = 0.0
      vt = 0.0
      work = 0.0
      sval = 0.0
      call dgesvd('A','A',m,n,aux,max(m,n),sval,u,
     &     m,vt,n,work,5*max(m,n),iRC)
      
      if(iRC.eq.0)then
        write(6,*)'SVD successful exit',iRC
      else if(iRC.lt.0)then
        write(6,*)'SVD error parameter:',abs(iRC)
      else if(iRC.gt.0)then
        write(6,*)'SVD did not converge:',iRC
      endif
      deallocate(aux)
      write(6,'(A)')'U MATRIX:'
      do i=1,m
        write(6,*)u(i,:)
      enddo
      write(6,'(A)')'VT MATRIX:'
      do i=1,n
        write(6,*)vt(i,:)
      enddo
      write(6,'(A)')'SINGULAR VALUES (EXCITATION AMPLITUDES):'
      write(6,*)sval(:)
      dum = 0.0d0
      do i=1, ldiag
        dum = dum + sval(i)**2
      enddo
      write(6,'(A)')'SUM OF AMPLITUDES:'
      write(6,*)dum
      nnto=0
      do i=1,ldiag
        if(sval(i).ge.thresh_NTO)nnto=nnto+1
      enddo
      write(6,'(A)')'CALCULATE NTOS'
      hole=0.0
      part=0.0
      if(nRas1.ge.nRas3)then
        do i=1,m
          do j=1,nBasFrag(iFrag)
            do k=1,m
              hole(i,j)=hole(i,j)+u(k,i) * gs(k+nInactFrag(iFrag),j)            
            enddo          
          enddo        
        enddo
      else
        do i=1,m
          do j=1,nBasFrag(iFrag)
            do k=1,m
              part(i,j)=part(i,j)+u(k,i) * gs(k+nInactFrag(iFrag)+n,j)            
            enddo          
          enddo        
        enddo
      endif
      
      if(nRas1.ge.nRas3)then
        do i=1,n
          do j=1,nBasFrag(iFrag)
            do k=1,n
              part(i,j)=part(i,j)+vt(i,k)*gs(k+nInactFrag(iFrag)+m,j)
            enddo
          enddo
        enddo    
      else
        do i=1,n
          do j=1,nBasFrag(iFrag)
            do k=1,n
              hole(i,j)=hole(i,j)+vt(i,k)*gs(k+nInactFrag(iFrag),j)
            enddo
          enddo
        enddo   
      endif
      do i=1,nRas1
        write(6,'(A,I3)')'OCCUPIED ORB',i
        write(6,*)hole(i,:)
      enddo
      do i=1,nRas3
        write(6,'(A,I3)')'VIRTUAL ORB',i
        write(6,*)part(i,:)
      enddo      
      allocate(sHP(nRas1,nRas3))
      sHP = 0.0
      do i=1,nRas1
        do j=1,nRas3
          do k=1,nBasFrag(iFrag)
            do l=1,nBasFrag(iFrag)
              sHP(i,j)=sHP(i,j)+hole(i,k)*part(j,l)*sAO(k,l)
            enddo
          enddo
        enddo
      enddo
      write(6,'(A)')'HOLE-PARTICLE OVERLAP:'
      do i=1,nRas1
        write(6,*)sHP(i,:)
      enddo
      deallocate(sHP)
      write(6,'(A)')'WRITE NTOS TO FILE:',ntofile      
      open(12,file=ntofile)
      write(12,'(A11)') '#INPORB 2.2'
      write(12,'(A5)')  '#INFO'
      write(12,'(A)')
     &     '* CIS excited state with active orbitals replaced by NTOs'
      write(12,'(3I8)')0,nSym,0
      write(12,'(I8)') nBasFrag(iFrag)
      write(12,'(I8)') nBasFrag(iFrag)
      write(12,'(A4)') '#ORB'
c     WRITE OCCUPIED INACTIVE ORBITALS
      do i=1,nInactFrag(iFrag)
        write(12,'(A9,2I5)')'* ORBITAL',i
        write(12,'(5E22.14)')(gs(i,j),j=1,nBasFrag(iFrag))
      enddo
c     WRITE OCCUPIED INACTIVE NTOS
      do i=1,nRas1-nnto
        write(12,'(A9,2I5)')'* ORBITAL',i+nInactFrag(iFrag)
        write(12,'(5E22.14)')(hole(i+nnto,j),j=1,nBasFrag(iFrag))
      enddo            
c     WRITE SIGNIFICANT HOLE NTOS
      do i=1,nnto
        write(12,'(A9,2I5)')'* ORBITAL',
     &       i+nInactFrag(iFrag)+nRas1-nnto
        write(12,'(5E22.14)')(hole(i,j),j=1,nBasFrag(iFrag))
      enddo
c     WRITE SIGNIFICANT PARTICLE NTOS
      do i=1,nnto
        write(12,'(A9,2I5)')'* ORBITAL',
     &       i+nInactFrag(iFrag)+nRas1
        write(12,'(5E22.14)')(part(i,j),j=1,nBasFrag(iFrag))
      enddo
c     WRITE UNOCCUPIED VIRTUAL NTOS
      do i=1,nRas3-nnto
        write(12,'(A9,2I5)')'* ORBITAL',i+nInactFrag(iFrag)+nRas1+nnto
        write(12,'(5E22.14)')(part(i+nnto,j),j=1,nBasFrag(iFrag))
      enddo
c     WRITE UNOCCUPIED AT THE END
      do i=nInactFrag(iFrag)+nActFrag(iFrag)+1,nBasFrag(iFrag)
        write(12,'(A9,2I5)')'* ORBITAL',i
        write(12,'(5E22.14)')(gs(i,j),j=1,nBasFrag(iFrag))
      enddo
      write(12,'(A4)') '#OCC'
      write(12,'(A20)') '* OCCUPATION NUMBERS'
      write(12,'(5E22.14)') (occNu(j),j=1,nBasFrag(iFrag))
      write(12,'(A)')'#INDEX'
      write(12,'(A)')'* 1234567890'
      occStr=''
      do i=1,nInactFrag(iFrag)
        occStr = trim(OccStr)//'i'
      enddo
      do i=1,nRas1-nnto
        occStr = trim(OccStr)//'i'
      enddo
      do i=1,nnto
        occStr = trim(OccStr)//'1'
      enddo
      do i=1,nnto
        occStr = trim(OccStr)//'3'
      enddo
      do i=1,nRas3-nnto
        occStr = trim(OccStr)//'s'
      enddo
      do i=1,nOrbFrag(iFrag)-nActFrag(iFrag)-nInactFrag(iFrag)
        occStr = trim(OccStr)//'s'
      enddo
      l=nOrbFrag(iFrag)/10
      if(mod(nOrbFrag(iFrag),10).ne.0)l=l+1
      do i=1,l
        k=10*(i-1)+1
        if(mod(i,10).ne.0)then
          write(12,'(I1,1x,A)')mod(i,10)-1,occStr(k:(k+9))
        else
          write(12,'(I1,1x,A)')9,occStr(k:(k+9))
        endif
      enddo
      close(unit=12)      
      write(6,'(A)')'CLOSE FILE:',ntofile
      write(6,'(A)')'WRITE NTO DETLIST TO FILE:',ntodetfile      
      open(13,file=ntodetfile)
      detheader = 0
      detheader(1) = nInactFrag(iFrag) + nRas1 - nnto
      write(13,'(8(I4))') (detheader(i),i=1,8)      
      do i=1,nnto        
        if(spin.eq.1)then
          occStr=''
          do j=1,nnto
            if(j.eq.i)then
              occStr = trim(occStr)//'b'
            else
              occStr = trim(occStr)//'2'
            endif
          enddo
          do j=1,nnto
            if(j.eq.i)then
              occStr = trim(occStr)//'a'
            else
              occStr = trim(occStr)//'0'
            endif
          enddo
          write(13,'(e15.8,6x,A)')sval(i),trim(occStr)
          occStr=''
          do j=1,nnto
            if(j.eq.i)then
              occStr = trim(occStr)//'a'
            else
              occStr = trim(occStr)//'2'
            endif
          enddo
          do j=1,nnto
            if(j.eq.i)then
              occStr = trim(occStr)//'b'
            else
              occStr = trim(occStr)//'0'
            endif
          enddo
          write(13,'(e15.8,6x,A)')sval(i)*(-1),trim(occStr)
        else if(spin.eq.3)then
          occStr=''
          do j=1,nnto
            if(j.eq.i)then
              occStr = trim(occStr)//'a'
            else
              occStr = trim(occStr)//'2'
            endif
          enddo
          do j=1,nnto
            if(j.eq.i)then
              occStr = trim(occStr)//'a'
            else
              occStr = trim(occStr)//'0'
            endif
          enddo
          write(13,'(e15.8,6x,A)')sval(i),trim(occStr)
        else
          write(6,'(A)')'spin configuration cannot
     &    be described with single excitations'
          write(6,'(A,I1)')'wrong spin:',spin
        endif
      enddo
      close(unit=13)
      write(6,'(A)')'CLOSE FILE:',ntodetfile
      open(14,file=gsdetfile)
      write(14,'(8(I4))') (detheader(i),i=1,8)
      occStr = ''
      do i=1,nnto
        occStr = trim(occStr)//'2'
      enddo
      do i=1,nnto
        occStr = trim(occStr)//'0'
      enddo
      write(14,'(e15.8,6x,A)')1.0d0,trim(occStr)
      close(unit=14)
      deallocate(tmatrix)
      deallocate(hole,part)
      deallocate(detcoef,detocc)
      deallocate(u,vt,work,sval)
      end subroutine getNTO            

      subroutine getAtomicOverlap(iFrag)
      use nto_data
      implicit none
      integer                           :: iFrag
      integer                           :: iCounter,iComponent,LuOne
      integer                           :: iOpt,iSymLbl,j,k
      integer                           :: lTriangle      
      real (kind=8),allocatable         :: s(:)
      
      call NameRun(runfile)
      lTriangle = (nBasFrag(iFrag)*(nBasFrag(iFrag)+1))/2
      allocate(s(lTriangle))      
      s = 0.0
      sAO = 0.0      
      iRc=-1
      iOpt=0
      luOne = 77
      Call OpnOne(iRC,iOpt,oneintfile,LuOne)
      if (iRC.ne.0) write(6,*)'Something went wrong opening',oneintfile
      iRC =  0
      iOpt = 2
      iComponent = 1
      iSymLbl = 1
      Call RdOne(iRC,iOpt,'Mltpl  0',iComponent,s,iSymLbl)
      iCounter = 1
      sAO = 0.0
      do j = 1, nBasFrag(iFrag)
        do k = 1, j
          sAO(j,k) = s(iCounter)
          sAO(k,j) = s(iCounter)
          iCounter = iCounter + 1
        end do
      end do
      iOpt = 0
      Call ClsOne(iRc,iOpt)
      deallocate(s)                
      return
      end subroutine getAtomicOverlap
      
      subroutine getFilenames(iFrag,iState)      
      use nto_data
      implicit none
      integer        :: iFrag,iState
      write(gsfile,'(A7,I1,A2)')'INPORB.',iFrag,'_1'
      write(excfile,'(A7,I1,A1,I1)')'INPORB.',iFrag,'_',iState
      write(detfile,'(A7,I1,A1,I1)')'vecdet.',iFrag,'_',iState
      write(ntofile,'(A8,I1,A1,I1)')'NTOFILE.',iFrag,'_',iState
      write(runfile,'(A6,I1)')'RUNFIL',iFrag
      write(oneintfile,'(A6,I1)')'ONEINT',iFrag      
      write(ntodetfile,'(A11,I1,A1,I1)')'NTODETFILE.',iFrag,'_',iState
      write(gsdetfile,'(A11,I1,A2)')'NTODETFILE.',iFrag,'_1'
      end subroutine getFilenames
      
      subroutine getnBas(iFrag)
      use nto_data
      implicit none
      integer       :: iFrag
      write(runfile,'(A6,I1)')'RUNFIL',iFrag
      call NameRun(runfile)      
      call Get_iScalar('nSym',nSym)
      if ( nSym .ne. 1 ) then
        write(*,*) '  Symmetry is not implemented in GronOR'
        write(*,*) 'Remove the symmetry elements and come back'
        stop
      end if
      call Get_iArray('nBas',nBasFrag(iFrag),nSym)
      end subroutine getnBas

      program nto_transformation
      use nto_data
      implicit none                        
      integer              :: i,iFrag,iState
      character(len=20)    :: inputFrag      
      character(len=20)    :: inputThresh
      character(len=20),allocatable    :: inputStates(:)

      call get_command_argument(1,inputFrag)      
      call get_command_argument(2,inputThresh)
      read(inputFrag,*) nFrag
      read(inputThresh,*) thresh_NTO
      allocate(inputStates(nFrag))
      allocate(nStatesFrag(nFrag))
      do i=1,nFrag
        call get_command_argument(i+2,inputStates(i))
        read(inputStates(i),*) nStatesFrag(i)
      enddo                  
      thresh_CI=0.0d0      
      write(6,'(A,I2)')'Number of fragments:',nFrag
      allocate(nBasFrag(nFrag))
      allocate(nOrbFrag(nFrag))
      allocate(nInactFrag(nFrag))
      allocate(nActFrag(nFrag))
      allocate(nDetFrag(nFrag))
      do iFrag = 1,nFrag
        call getnBas(iFrag)
      enddo      
      do iFrag = 1,nFrag        
        allocate(gs(nBasFrag(iFrag),nBasFrag(iFrag)))
        allocate(exc(nBasFrag(iFrag),nBasFrag(iFrag)))        
        allocate(sAO(nBasFrag(iFrag),nBasFrag(iFrag)))
        allocate(occNu(nBasFrag(iFrag)))
        do iState = 2,nStatesFrag(iFrag)
          gs = 0.0
          exc = 0.0
          sAO = 0.0
          occNu = 0
          call getFilenames(iFrag,iState)
          call getAtomicOverlap(iFrag)
          call read_vecs(iFrag)
          write(6,'(A)')'Vectors have been read and stored'
          call getNTO(iFrag,iState)
          write(6,'(A)')
     &         'Natural transition orbitals have been calculated'
          write(6,'(A,I2,2X,I2)')'Original active space:',nRas1,nRas3
          write(6,'(A,I2)')'Number of significant NTO pairs:',nnto
        enddo
        deallocate(gs,exc,sAO,occNu)
      enddo
      deallocate(nStatesFrag,nBasFrag,nOrbFrag)
      deallocate(nInactFrag,nActFrag,nDetFrag)

      end program nto_transformation
